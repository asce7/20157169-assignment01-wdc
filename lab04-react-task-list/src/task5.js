const users = [
    {
       uid: 001,
       email: 'john@dev.com',
       personalInfo: {
          name: 'John',
          address: {
             line1: 'westwish st',
             line2: 'washmasher',
             city: 'wallas',
             state: 'WX'
          }
       }
    },
    {
       uid: 063,
       email: 'a.abken@larobe.edu.au',
       personalInfo: {
          name: 'amin',
          address: {
             line1: 'Heidelberg',
             line2: '',
             city: 'Melbourne',
             state: 'VIC'
          }
       }
    },
    {
       uid: 045,
       email: 'Linda.Paterson@gmail.com',
       personalInfo: {
          name: 'Linda',
          address: {
             line1: 'Cherry st',
             line2: 'Kangaroo Point',
             city: 'Brisbane',
             state: 'QLD'
          }
       }
    }
 ]
 function returnUsers(users)
 {
    var listOfUsers = []
    for(var i= 0; i < users.length; i++)
    {
        var currentUser = {name: users[i].personalInfo.name, email: users[i].email, state: users[i].personalInfo.address.state} ; 
        listOfUsers.push(currentUser) ; 
    }
    return listOfUsers;
}
     _
function showUsers()
{
    var listOfUsers = returnUsers(users) ; 
    html = "<table style= 'width:100%'>" ;
    html += "<tr> <th>Name</th> <th>Email</th> <th>State</th>" ;
    for(var i = 0 ; i < listOfUsers.length ; i++)
    {
        html += "<td>" + listOfUsers[i].name + "</td>" ;
        html += "<td>" + listOfUsers[i].email + "</td>" ;
        html += "<td>" + listOfUsers[i].state + "</td>" ;
    }
    html += "</tr> </table>" ;
    document.getElementById('tableId').innerHTML = html ;  
}

function sortedUsers()
{
   var listOfUsers = returnUsers(users) ; 
   listOfUsers.sort((a,b) => a.name.localeCompare(b.name)) ; 
   return listOfUsers ; 
}